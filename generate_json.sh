#!/bin/bash

function generate_json {
    dir=$1
    output=$2
    build_dir="build"

    # Ensure the build directory exists
    mkdir -p "$build_dir"

    echo "[" > "$build_dir/$output"

    first=true
    for file in "$dir"/*; do
        if [ "$first" = true ] ; then
            first=false
        else
            echo "," >> "$build_dir/$output"
        fi

        filename=$(basename -- "$file")
        filename_no_ext="${filename%.*}"
        id_lower=$(echo "$filename_no_ext" | tr '[:upper:]' '[:lower:]')

        echo "{
            \"id\": \"$id_lower\",
            \"path\": \"$file\"
        }" >> "$build_dir/$output"
    done

    echo "]" >> "$build_dir/$output"
}

generate_json "chains" "chains.json"
generate_json "tokens" "tokens.json"
generate_json "exchanges" "exchanges.json"
generate_json "bridges" "bridges.json"
